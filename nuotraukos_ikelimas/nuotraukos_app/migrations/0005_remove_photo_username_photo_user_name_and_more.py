# Generated by Django 4.0.4 on 2022-05-15 19:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nuotraukos_app', '0004_photo_username_photoup_username_alter_photoup_file'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='photo',
            name='username',
        ),
        migrations.AddField(
            model_name='photo',
            name='user_name',
            field=models.CharField(default=1, max_length=200, verbose_name=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='photoup',
            name='username',
            field=models.CharField(max_length=200, verbose_name=''),
        ),
    ]
