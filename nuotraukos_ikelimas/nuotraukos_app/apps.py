from django.apps import AppConfig


class NuotraukosAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nuotraukos_app'
