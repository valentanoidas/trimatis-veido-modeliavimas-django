from django.urls import path
from . import views

urlpatterns = [
    path('about', views.about, name='about'),
    path('threejs',views.threejs, name='threejs'),
    path('upload',views.upload, name='upload'),
    path('webcam',views.webcam, name='webcam'),
    path('', views.index, name='index'),
]
