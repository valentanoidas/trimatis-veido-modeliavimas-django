
    // CAMERA SETTINGS.


    Webcam.set({
        width: 500,
        height: 375,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach('#camera');

    var shutter = new Audio();
    shutter.autoplay = false;
    shutter.src = navigator.userAgent.match(/Firefox/) ? 'static/shutter.ogg' : 'static/shutter.mp3';

    function takeSnapShot() {
    // play sound effect
    shutter.play();

    // take snapshot and get image data
    Webcam.snap( function(data_uri) {
      // display results in page
      document.getElementById('results').innerHTML =
        '<img id="imageprev" src="'+data_uri+'"/>';
    } );
    document.getElementById("camera").outerHTML = "";

    Webcam.reset();
  }

  function saveSnapShot(){
  // Get base64 value from <img id='imageprev'> source
  var base64image =  document.getElementById("imageprev").src;

   Webcam.upload( base64image, 'static/upload.php', function(code, text) {
     console.log('Save successfully');
     //console.log(text);
        });

  }
