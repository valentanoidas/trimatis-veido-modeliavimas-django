from django import forms
from nuotraukos_app.models import Photo, photoup

class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ['webcam_file','username']

class photoupForm(forms.ModelForm):
    class Meta:
        model = photoup
        fields = ['file','username']