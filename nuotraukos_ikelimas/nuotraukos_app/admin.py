from django.contrib import admin
from .models import photoup, Photo
# Register your models here.

admin.site.register(photoup)
admin.site.register(Photo)