import * as THREE from 'https://unpkg.com/three@0.108.0/build/three.module.js';
import { OBJLoader } from 'https://unpkg.com/three@0.108.0/examples/jsm/loaders/OBJLoader.js';
import {OrbitControls} from 'https://unpkg.com/three@0.108.0/examples/jsm/controls/OrbitControls.js';


//String currentpath = System.getProperty("user.dir");
//String path = FileSystems.getDefault().getPath(".").toString();
//var path = require("path");
//require(path.join(__dirname, '/models'));
//alert("Working Directory = ");

// Scene

const scene = new THREE.Scene();

const light = new THREE.DirectionalLight( 0xffffff, 1 );
light.position.set( 0, 0, 10000 );

scene.add(light)

const camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.1, 100000 );

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

//const axesHelper = new THREE.AxesHelper( 1000 );
//scene.add( axesHelper );

// Controls

const controls = new OrbitControls( camera, renderer.domElement );
camera.position.set( 0, 0, 2800 );
controls.enableDamping = true
controls.update();

// Get filename from html 
var jpg_filename = document.getElementById("filename").getAttribute("data-jpg_filename");
//var filena = document.getElementById("filename");
//var filenam = filena.files[0].name;
let obj_filename = jpg_filename.slice(0, -4);
//alert(obj_filename + "_0.obj");

//Loader

const loader = new OBJLoader();

loader.load( 'media/'+ obj_filename + "_0.obj", 

function ( OBJ ) {
        
    OBJ.rotation.z = -(Math.PI / 2);// apsuka modeli -90 lapsniu pagal Z koordinates asi
        
    var boundingBox = new THREE.Box3().setFromObject( OBJ );
    boundingBox.getCenter( OBJ.position ).negate();// Padaro modeli (0,0,0) koordinatese.
        
    scene.add( OBJ );
            
            
         OBJ.traverse(node => {
            if (node.material) {
            node.material.side = THREE.BackSide;//Pakeiciama modelio puse
            node.material.vertexColors = true;//teksturos atvaizdavimas is .obj vertex
            }
        });
            
},
        
        

function ( xhr ) {

    console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

},
    

function ( error ) {

    console.log( 'An error happened' );

}
);

        
// animate
        
function animate() {
    requestAnimationFrame( animate );
        //console.log(camera.position.x + '  x');
        //console.log(camera.position.y + '  y ');
        //console.log(camera.position.z + '  z ');
    controls.update();
        
    renderer.render( scene, camera );
}
animate();