from django.db import models

# Create your models here.

class photoup(models.Model):
    file = models.FileField(upload_to='')
    username = models.CharField(max_length=200, verbose_name="")

class Photo(models.Model):
    webcam_file = models.CharField(max_length = 100000)
    username = models.CharField(max_length = 200)
    
class History(models.Model):
    file_jpg = models.CharField(max_length=200)
    file_crop = models.CharField(max_length=200)
    file_obj = models.CharField(max_length=200)
    username = models.CharField(max_length=200)

