from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import photoup, History
from .forms import PhotoForm, photoupForm
import os
from os import listdir
from django.conf import settings
import datetime
from django.contrib import messages
import time

# Create your views here.
def index(request):
    return render(request, 'index.html',{})

@login_required
def upload(request):
    form=photoupForm
    if request.method == 'POST':
        new_photo = photoup(
            file = request.FILES['file'],
            username = request.POST['username'],
        )
        new_photo.save()

        filen= new_photo.file.name.split('/')[-1]
        #os.system('echo '+new_photo)
        #pathlib.Path.cwd()
        os.system('cd ..\ncd cleardusk/3DDFA\npython main.py -f ../../nuotraukos_ikelimas/media/'+filen+'\ncd ../../nuotraukos_ikelimas/')
        #time.sleep(3)

        file_exists = os.path.exists('media/'+os.path.splitext(filen)[0]+'_0.obj')
        

        if file_exists:
            os.remove('media/'+os.path.splitext(filen)[0]+'_0_paf.jpg')
            history = History(
                file_jpg = filen,
                file_crop = os.path.splitext(filen)[0]+'_0_crop.jpg',
                file_obj = os.path.splitext(filen)[0]+'_0.obj',
                username = new_photo.username,
            )
            history.save()

            return render(request, 'threejs.html',{"filen": filen})

        else:
            messages.warning(request, f'Face was not detected in {filen}! Please try again. Nuotraukoje {filen} neaptiktas veidas! Bandykite is naujo')

            return redirect('upload')
        
        #filen = request.FILES['file'].name
        #os.system('echo '+history.file_crop)        

    else:
        return render(request, 'upload.html', {"form":form})

def about(request):
    return render(request, 'about.html')

"""def skriptas(request):
	if request.method == 'POST': 
		subprocess.call('/home/velerionas/cleardusk_3ddfa_django/cleardusk/3DDFA/run.sh')
		return render(request, 'skriptas.html')
	else :
		return render(request, 'upload.html')"""

def threejs(request):
    return render(request, 'threejs.html')

@login_required
def webcam(request):
    form = PhotoForm
    if request.method == 'POST':
        
        form = PhotoForm(request.POST or None)
        form2=form.save(commit=False)
        data_post=request.POST.get('webcam_file')
        data_post2=request.POST.get('username')
        #os.system('echo '+data_post2)
        datetimevar0 = datetime.datetime.now()
        datetimevar = datetimevar0.strftime("%H:%M:%S.%f - %b %d %Y").replace(" ", "")
        filen = 'webcamera_'+datetimevar + '.jpg'
        form2.username = data_post2
        form2.webcam_file = filen
        
        form2.save()

        history = History(
            file_jpg = filen,
            file_crop = filen[:-4]+'_0_crop.jpg',
            file_obj = filen[:-4]+'_0.obj',
            username = data_post2,
        )
        history.save()
       
        os.system('cd media\necho "'+ data_post[23:] +'" | base64 -d > '+filen+'\ncd ..') #issaugo foto media/*.jpg faila
        os.system('cd ..\ncd cleardusk/3DDFA\npython main.py -f ../../nuotraukos_ikelimas/media/'+filen+'\ncd ../../nuotraukos_ikelimas/')
        os.remove('media/'+filen[:-4]+'_0_paf.jpg') #istrina foto
        #os.system('echo '+filen)       
        return render(request, 'threejs.html', {"filen":filen})
        
        #file=request.FILES['img']
        #fs=FileSystemStorage()
        #fs.save(datetime.datetime.now(), file)
        #return render(request, 'index.html')
    else:
        return render(request, 'webcam.html', {'form':form})




    














