from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from users_app import views as users_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('about/', include('nuotraukos_app.urls')),
    path('upload/', include('nuotraukos_app.urls')),
    path('threejs/', include('nuotraukos_app.urls')),
    path('webcam/', include('nuotraukos_app.urls')),
    path('', include('nuotraukos_app.urls')),
    path('history/', users_views.history, name='history'),
    path('register/', users_views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),
    
]

urlpatterns=urlpatterns + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT) 

urlpatterns=urlpatterns + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
