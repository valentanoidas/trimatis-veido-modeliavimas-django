from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm
import os
from nuotraukos_app.models import History
from django.conf import settings


# Create your views here.
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}! You can log in now.')
            return redirect('login')
        else:
            return render(request, 'register.html',{'form': form})
    else:
        form = UserRegisterForm()
        return render(request, 'register.html',{'form': form})

@login_required
def history(request):
    if request.method == 'POST':
        filen=request.POST.get('obj')
        #os.system('echo '+filen)
        return render(request, 'threejs.html', {"filen":filen})
    
    else:
        photo_data = History.objects.filter(username=f'{request.user.username}') 
        path = settings.MEDIA_ROOT  
        return render(request, 'history.html',{'all':photo_data})


    