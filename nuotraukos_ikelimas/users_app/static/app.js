
    // CAMERA SETTINGS.

    //Webcam.set({
    //  width: 400,
    //  height: 400,
    //  dest_width: 720,
    //  dest_height: 720,
    //  crop_width: 720,
    //  crop_height: 720,
    //  force_flash: false
    //});
    if (screen.height <= screen.width) {
      // Landscape
              Webcam.set({
          width: 500,
          height: 375,
      });

    } 
    else {
      // Portrait
              Webcam.set({
          width: 375,
          height: 500,
      }); 
    }

    navigator.mediaDevices.enumerateDevices().then((devices) => {
      let videoSourcesSelect = document.getElementById("video-source");

      // Iterate over all the list of devices (InputDeviceInfo and MediaDeviceInfo)
      devices.forEach((device) => {
        let option = new Option();
        option.value = device.deviceId;

        // According to the type of media device
        switch(device.kind){
          // Append device to list of Cameras
          case "videoinput":
              option.text = device.label || `Camera ${videoSourcesSelect.length + 1}`;
              videoSourcesSelect.appendChild(option);
          break;
        }
        console.log(device);
      });
    }).catch(function (e) {
      console.log(e.name + ": " + e.message);
    });
    //Webcam.set({
    //    width: 500,
    //    height: 375,
    //    image_format: 'jpeg',
    //    jpeg_quality: 90,
    //    constraints: {
    //      video: true,
    //      facingMode: "environment"
    //    }
    //});
    Webcam.attach('#camera');

    var shutter = new Audio();
    shutter.autoplay = false;
    shutter.src = navigator.userAgent.match(/Firefox/) ? 'static/shutter.ogg' : 'static/shutter.mp3';

    function takeSnapShot() {
    // play sound effect
    shutter.play();

    // take snapshot and get image data
    Webcam.snap( function(data_uri) {
      // display results in page
      document.getElementById('results').innerHTML =
        '<img id="imageprev" src="'+data_uri+'"/>';
    } );
    document.getElementById("camera").outerHTML = "";

    Webcam.reset();
  }

  function saveSnapShot(){
  // Get base64 value from <img id='imageprev'> source
  var base64image =  document.getElementById("imageprev").src;

   Webcam.upload( base64image, 'static/upload.php', function(code, text) {
     console.log('Save successfully');
     //console.log(text);
        });

  }
