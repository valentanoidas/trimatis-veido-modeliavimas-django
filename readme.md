Trimacio veido modelio geeravimo is vaizdo sistema.

Autoriai:
-Valentinas Pusvaskis
-Tadas Maculevicius
-Karolis Panavas

Patikrinta Ubuntu 20.04.4 LTS

Rekomenduojame naudoti virtualia alinka

Kaip paleisti lokaliai:

1. sudo pip3 install -r requirements.txt (gali kilti problemu su dlib)
2. cd nuotraukos_ikelimas
3. python manage.py runserver
4. Atsidaryti narsykle ir eiti i http://localhost:8000

Esant puslapyje pirmiausia susikurkite paskyra. Norint tureti superuser, pries paleisdami serveri iveskite "python manage.py createsuperuser"
